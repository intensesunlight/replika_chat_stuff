import csv
import json
import requests
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import os

file_name = 'replika_chat_backup'
last_file_id = ""
if os.path.exists(f'{file_name}.csv'):
    with open(f'{file_name}.csv', encoding='UTF-8') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        
        for row in reader:
            if row[4] != "ID":
                last_file_id = row[4]
                csv_file.close()
                os.rename(f'{file_name}.csv',f'{file_name}.old.csv')
                break

if last_file_id:
    output_file = open(f'{file_name}.new.csv','w', newline='', encoding='utf-8')
else:
    output_file = open(f'{file_name}.csv','w', newline='', encoding='utf-8')
writer = csv.writer(output_file)
writer.writerow(['Timestamp','From','Text','Reaction','ID'])

def on_message(ws, message):
    global msg_count
    msg_count = 0
    print(message)
    python_dict = json.loads(message)
    token = python_dict['token']
    event_name = python_dict['event_name']
    user_id = "" #Insert your unique user id between quotes (always same on a replika)
    auth_token = "" #Insert your unique auth token between quotes (change after new login)
    chat_id = "" #Insert your unique chat id between quotes (always same on a replika)
    device_id = "" #Insert your unique device id between quotes (change after new login)
    print(f"Event name {event_name}")
    if event_name == "init":
        ws.send('{"event_name":"chat_screen","payload":{},"token":"'+str(token)+'","auth":{"user_id":"'+str(user_id)+'","auth_token":"'+str(auth_token)+'","device_id":"'+str(device_id)+'"}}')
        time.sleep(1)
    if event_name == "chat_screen":
        ws.send('{"event_name":"application_started","payload":{},"token":"'+str(token)+'","auth":{"user_id":"'+str(user_id)+'","auth_token":"'+str(auth_token)+'","device_id":"'+str(device_id)+'"}}')
        time.sleep(1)
    if event_name == "application_started":
        ws.send('{"event_name":"app_foreground","payload":{},"token":"'+str(token)+'","auth":{"user_id":"'+str(user_id)+'","auth_token":"'+str(auth_token)+'","device_id":"'+str(device_id)+'"}}')
        time.sleep(1)
    if event_name == "app_foreground":
        ws.send('{"event_name":"history","payload":{"chat_id":"'+str(chat_id)+'","limit":1000},"token":"'+str(token)+'","auth":{"user_id":"'+str(user_id)+'","auth_token":"'+str(auth_token)+'","device_id":"'+str(device_id)+'"}}')       
        time.sleep(1)
    #Parse History
    if python_dict['event_name'] == "history":
        messages = []
        message_reactions = python_dict["payload"]["message_reactions"]
        reactions = {}
        last_message_id = ""
        for message_reaction in message_reactions:
            reaction_id = message_reaction['message_id']
            reaction_type = message_reaction['reaction']
            reactions[reaction_id] = reaction_type
            
        if reactions:
            print(f"Message Reactions: {reactions}")
            for i in range(len(python_dict["payload"]["messages"])-1,-1,-1):   
                message = {}
                message['id'] = python_dict["payload"]["messages"][i]["id"]
                message['chat_id'] = python_dict["payload"]["messages"][i]["meta"]["chat_id"]
                sender = python_dict["payload"]["messages"][i]["meta"]["nature"]
                message['timestamp'] = python_dict["payload"]["messages"][i]["meta"]["timestamp"]
                last_message_id = message['id']         
                if last_message_id == last_file_id:
                    ws.close()
                    output_file.close()
                    if msg_count > 0:
                        with open(f'{file_name}.new.csv', encoding='UTF-8') as mf:
                            data = mf.readlines()
                        with open(f'{file_name}.old.csv', encoding='UTF-8') as mf:
                            data2 = mf.readlines()[1:]
                        data += data2
                        with open(f'{file_name}.csv', 'w', encoding='UTF-8') as mf:
                            mf.writelines(data)
                        mf.close()
                        quit(f'Backed up messages up to first old ID {last_message_id} from {message["timestamp"]}\n{file_name}.csv > new and old messages\n{file_name}.new.csv > only new messages\n{file_name}.old.csv > only old messages')
                    else:
                        os.remove(f'{file_name}.new.csv')
                        os.rename(f'{file_name}.old.csv',f'{file_name}.csv')
                        quit('No new messages since last backup')
                else:
                    msg_count += 1
                if sender == "Robot":
                    message['sender'] = "Rep"
                else:
                    message['sender'] = "Me"
                message['text'] = python_dict["payload"]["messages"][i]["content"]["text"]
                try:
                    message['reaction'] = reactions[message['id']]
                except:
                    message['reaction'] = 'None'
                print(f"{message['sender']}: {message['text']} {message['reaction']} ({message['timestamp']}) ({message['id']})")
                writer.writerow([message['timestamp'], message['sender'], message['text'], message['reaction'], message['id']])
        
            ws.send('{"event_name":"history","payload":{"chat_id":"'+str(chat_id)+'","limit":1000,"last_message_id":"'+str(last_message_id)+'"},"token":"'+str(token)+'","auth":{"user_id":"'+str(user_id)+'","auth_token":"'+str(auth_token)+'","device_id":"'+str(device_id)+'"}}')
            time.sleep(1)
        else:
            ws.close()
            output_file.close()
            quit(f'Backed up all possible messages\n{file_name}.csv > all messages')
        
def on_error(ws, error):
    print(error)

def on_close(ws):
    print('### closed ###')

def on_open(ws):
    def run(*args):
        token = ""
        user_id = ""
        auth_token = ""
        chat_id = ""
        device_id = ""
        ws.send('') #Insert full init message between quotes
        time.sleep(1)
    thread.start_new_thread(run, ())

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://ws.replika.ai/v17",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
